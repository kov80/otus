import unittest

from parsers.filter_parser import FilterParser


class TestFilterParser(unittest.TestCase):
    def test_parse(self):
        parser = FilterParser(['1', '3', '5'])
        parsed_data = parser.parse({'1': 1, '2': 2, '3': 3, '4': 4, '5': 5})
        self.assertEqual(len(parsed_data), 1)
        self.assertDictEqual(parsed_data[0], {'1': 1, '3': 3, '5': 5})


class TestHtmlParser(unittest.TestCase):

    def test_parse(self):
        from parsers.html_parser import HtmlParser

        parser = HtmlParser([])
        with open('data/test.html', 'rb') as f:
            res = parser.parse(f.read().decode())

        test_res = {
            'num': '1',
            'id': '3927',
            'name': 'Вино Микитани Пиросмани стол.кр.п/сух',
            'name_en': 'Mikitani Пиросмани',
            'volume': '0.75', 'country': 'Грузия',
            'price': '299.9',
            'color': 'Красное',
            'alcohol': '11',
            'temperature': '14',
            'sugar': 'П/сухое', 'manufacturer':
            'Corporation Georgian Wine',
            'region': '',
            'sort': 'Саперави',
            'rating_name': '',
            'rating': '',
            'votes_rating': '3',
            'votes_total': '273',
            'description': 'Пиросманиi— мягкое, бархатистое вино с богатым фруктовым ароматом и гармоничным, '
                           'сбалансированным вкусом. Оно создано из красного сорта винограда Саперави, выращенного в '
                           'знаменитом винном регионе Кахетия, в восточной Грузии. Вино рекомендуется подавать к '
                           'мясным блюдам, также хорошо сочетается с десертами и фруктами. Желательно употреблять '
                           'охлажденным до +12-14 °C.', 'taste': 'Вкус гармоничный с приятной терпкостью.',
            'product': 'Сыр,Мясо,Паста,Птица',
            'reviews': [
                {'num': '1', 'wine_id': '3927', 'rating': '1', 'reviewer': 'Виктор',
                 'text': 'Это не вино. Проверили, опустив рюмку в воду - все растворилось, '
                         'а в рюмке осталась еле розовая бурда. На вкус тоже дрянь.'},
                {'num': '2', 'wine_id': '3927', 'rating': '5', 'reviewer': 'Алексей',
                 'text': 'Отличное вино!!!!'}]
        }

        assert (res == test_res)


if __name__ == '__main__':
    unittest.main()
