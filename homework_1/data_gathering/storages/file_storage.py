import os

from storages.storage import Storage


class FilesStorage(Storage):
    def __init__(self, path, ext=''):
        os.makedirs(path, exist_ok=True)
        self.path = path
        self.ext = ext

    def read_data(self):
        for file_name in os.listdir(self.path):
            with open(os.path.join(self.path, file_name), 'rb') as f:
                yield f.read().decode()

    def write_data(self, data_dict):
        """
        :param data_dict: dictionary of strings that
        should be written as files
        """
        for name, data in data_dict.items():
            with open(os.path.join(self.path, name + self.ext), 'wb') as f:
                f.write(data.encode())

    def append_data(self, data_dict):
        """
        :param data_dict: dictionary of strings that
        should be written as files
        """
        self.write_data(data_dict)

    def exists_data(self, name):
        return os.path.exists(os.path.join(self.path, name + self.ext))


class CsvStorage(Storage):

    def __init__(self, file_name):
        self.init = False
        self.file_name = file_name

    def append_data(self, data):
        """
        :param data: string
        """
        if not self.init:
            self.init = True
            with open(self.file_name, 'wb') as f:
                f.write('\t'.join(data.keys()).encode() + b'\n')
        with open(self.file_name, 'ab') as f:
            f.write('\t'.join(data.values()).encode() + b'\n')
