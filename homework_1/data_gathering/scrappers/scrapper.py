import logging
import requests
import time
import os

from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class Scrapper(object):
    SITE = 'https://krasnoeibeloe.ru'
    HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'}
    COOkIES = {'ageConfirmed': 'true', 'promoShowed4': 'true', 'userCityId': '878'}
    FILTER = '?view_mod=list&arrFilter_100_MIN=0.5&arrFilter_100_MAX=1.0&arrFilter_103_MIN=6&arrFilter_103_MAX=19&' \
             'sort_by=price_asc&page_count=48&set_filter=Y'

    catalogs = ['/catalog/vino/', '/catalog/__2/', '/catalog/vino-s-otsenkoy/']
    result = {'added': 0, 'skipped': 0, 'error': 0}
    count = 0

    def __init__(self, storage, skip_objects=None):
        self.skip_objects = skip_objects
        self.storage = storage

    def scrap_wine_page(self, path):
        name = os.path.basename(os.path.normpath(path))
        if self.skip_objects and self.storage.exists_data(name):
            logger.warning(' - skip existing: %s' % name)
            self.result['skipped'] += 1
            return

        page_num = 1
        name_i = name
        while path:
            url = '%s%s' % (self.SITE, path)
            response = requests.get(url, headers=self.HEADERS, cookies=self.COOkIES)
            if not response.ok:
                logger.error(' - error: %s' % name)
                self.result['error'] += 1
                logger.error(response.text)
                # then continue process, or retry, or fix your code
            else:
                soup = BeautifulSoup(response.text)
                card = soup.find('div', {'class': 'product_card_container'})

                wine_name = card.find('div', {'class': 'product_card_name'}).text.strip()
                if any(kind in wine_name.lower() for kind in ('фруктовое', 'напиток')):
                    logger.warning(' - skip not wine: %s' % name)
                    self.result['skipped'] += 1
                    return

                self.count += 1
                logger.info(' - add (%s): %s' % (self.count, name_i))
                self.result['added'] += 1
                self.storage.append_data({name_i: response.text})

                pagination = soup.find('div', {'class': 'bl_pagination'})
                if pagination:
                    next_page_class = pagination.find('li', {'class': 'pag_arrow_right'})
                    path = next_page_class.find('a').get('href')
                    if path.startswith('javascript'):
                        path = None
                    else:
                        page_num += 1
                        name_i = '%s_%s' % (name, page_num)
                else:
                    path = None

                time.sleep(0.1)

    def scrap_process(self):

        # You can iterate over ids, or get list of objects
        # from any API, or iterate throught pages of any site
        # Do not forget to skip already gathered data
        # Here is an example for you

        catalog_i = 0
        next_page = None
        while catalog_i < len(self.catalogs):
            url = '%s%s%s' % (self.SITE, next_page if next_page else self.catalogs[catalog_i], self.FILTER)
            response = requests.get(url, headers=self.HEADERS, cookies=self.COOkIES)

            if not response.ok:
                logger.error(response.text)
                # then continue process, or retry, or fix your code

            else:
                # Note: here json can be used as response.json
                soup = BeautifulSoup(response.text)

                wine_list = soup.find('div', {'class': 'catalog_container catalog_container_view_list'})
                wine_images = wine_list.find_all('div', {'class': ['product_item_images']})

                for w in wine_images:
                    self.scrap_wine_page(w.find('a').get('href'))

                pagination = soup.find('div', {'class': 'bl_pagination'})
                if pagination:
                    next_page_class = pagination.find('li', {'class': 'pag_arrow_right'})
                    next_page = next_page_class.find('a').get('href')
                    if next_page.startswith('javascript'):
                        next_page = None

                if not next_page:
                    catalog_i += 1

        return self.result

